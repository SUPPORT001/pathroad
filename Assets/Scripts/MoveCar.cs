using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCar : Entity
{
    internal bool KeyA = false;
    internal bool KeyD = false;
    internal bool KeyW = false;
    internal bool KeyS = false;
    private float rot_speed;
    private float bst_speed;
    public bool play = false;
    public GameObject Player;
    //private float timer;

    private void FixedUpdate()
    {
        if (play)
        {
            if (Input.GetKey(KeyCode.A))
            {
                rot_speed = GetComponent<Step>().rotate_speed[GetComponent<Step>().num];
                Player.transform.position -= new Vector3(rot_speed, 0, 0);
            }
            if (Input.GetKey(KeyCode.D))
            {
                rot_speed = GetComponent<Step>().rotate_speed[GetComponent<Step>().num];
                Player.transform.position += new Vector3(rot_speed, 0, 0);
            }
            if (Input.GetKey(KeyCode.W))
            {
                bst_speed = GetComponent<Step>().byst_speed[GetComponent<Step>().num];
                Player.transform.position += new Vector3(0, bst_speed, 0);
            }
            if (Input.GetKey(KeyCode.S))
            {
                Player.transform.position -= new Vector3(0, bst_speed, 0);
            }
        }
        /*if (Player.transform.position.y > -0.5f)
        {
            timer = 0;
            if (Input.GetKeyUp(KeyCode.W))
            {

            }
        }*/
    }

    
}
