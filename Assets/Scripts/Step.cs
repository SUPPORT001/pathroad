using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Step : MonoBehaviour
{
    public List<Sprite> sprites;
                                                     //Police / Light / Hight
    private List<float> speed = new List<float> { 3.5f, 1.8f, 0.8f };
    public List<float> rotate_speed = new List<float> { 0.04f, 0.025f, 0.01f };
    public List<float> byst_speed = new List<float> { 0.035f, 0.02f, 0.015f };
    public int num;
    public GameObject auto;
    private float timer = 0;
    public Text txt;

    private void Start()
    {
        GameObject go = this.transform.GetChild(1).gameObject;
        go.SetActive(true);
        go = this.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
        go.GetComponent<Animator>().enabled = false;
    }

    public void AutoStart(int number)
    {
        num = number;
        auto.GetComponent<Image>().sprite = sprites[number];
        GameObject go = this.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
        Animator anim = go.GetComponent<Animator>();
        anim.speed = speed[number];
        go = this.transform.GetChild(1).gameObject;
        go.SetActive(false);
        anim.GetComponent<Animator>().enabled = true;
        auto.SetActive(true);
        GetComponent<MoveCar>().play = true;
    }

    private void Update()
    {
        if (GetComponent<MoveCar>().play)
        {
            timer += Time.deltaTime;
            
        }
        else
        {
            timer = 0;
        }
    }
}
